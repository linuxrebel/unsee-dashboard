image: alpine:latest

variables:
  AUTO_DEVOPS_DOMAIN: epc.erikube.io

stages:
    - deploy

deploy:
    stage: deploy
    script:
        - set_deploy_variables
        - check_kube_domain
        - install_dependencies
        - set_kube_config
        - build_chart
        - ensure_namespace
        - install_tiller
        - deploy
    environment:
        name: $CI_COMMIT_REF_SLUG
        url: http://$CI_PROJECT_PATH_SLUG.$CI_COMMIT_REF_SLUG.$AUTO_DEVOPS_DOMAIN
    only:
        refs:
            - dev
            - prod
            - stag
        kubernetes: active

.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  export TILLER_NAMESPACE=$KUBE_NAMESPACE

  hostname=`echo $CI_ENVIRONMENT_URL | sed 's/http:\/\///'`

  function deploy() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"
    echo $name

    helm upgrade --install \
      --wait \
      --set alertmanager.uri="http://$CI_COMMIT_REF_SLUG-prometheus-alertmanager.prometheus-$CI_COMMIT_REF_SLUG.svc.cluster.local" \
      --set alertmanager.uriPort="80" \
      --set ingress.annotations.kubernetes\\.io/ingress\\.class="nginx" \
      --set ingress.annotations.kubernetes\\.io/tls-acme="\"true\"" \
      --set ingress.enabled="true" \
      --set ingress.hosts[0]=$hostname \
      --set ingress.tls[0].hosts[0]=$hostname \
      --set ingress.tls[0].secretName=$CI_ENVIRONMENT_NAME-auto-deploy-tls \
      --namespace="$KUBE_NAMESPACE" \
      --version="$CI_PIPELINE_ID-$CI_JOB_ID" \
      "$name" \
      unsee/
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.23-r3/glibc-2.23-r3.apk
    apk add glibc-2.23-r3.apk
    rm glibc-2.23-r3.apk

    curl https://kubernetes-helm.storage.googleapis.com/helm-v2.6.1-linux-amd64.tar.gz | tar zx
    mv linux-amd64/helm /usr/bin/
    helm version --client

    curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function set_kube_config() {
    if [ "$CI_COMMIT_REF_NAME" == "prod" ]; then
      url="https://35.235.80.58"
      cert="-----BEGIN CERTIFICATE-----\nMIIDDDCCAfSgAwIBAgIRAPoLR97if+9YhzGOS2ocw0owDQYJKoZIhvcNAQELBQAw\nLzEtMCsGA1UEAxMkMmY2NDJmOTUtZGNjNS00NDM3LWE0YjUtYTQ3YmI3YzU2M2Ex\nMB4XDTE4MDgyMzE0Mzk0MloXDTIzMDgyMjE1Mzk0MlowLzEtMCsGA1UEAxMkMmY2\nNDJmOTUtZGNjNS00NDM3LWE0YjUtYTQ3YmI3YzU2M2ExMIIBIjANBgkqhkiG9w0B\nAQEFAAOCAQ8AMIIBCgKCAQEA5bK92O2+p3wkoLVMDKXXoeyiprI31QD/n22IB704\nMHKwPeS77OzQVTuJzzJeoo+CvdNCXyJwJllGQ9N093WRQ1AuYAlBqMWWnWXWF+2h\nKTBcZ210iEqiCydgEY0KPxlRnSGAnbMz/03XZttvKOENK4zUHtSAtwOrMpfLL+nF\nIxCdPPSOAQ+gC8pJWUG5TXE/7/kJ6IpTOU1dCfInqGB4PaoxXAEy/XHsyyjwAxej\nwm6VMHHm0WxgOG1WuDJXHb8kf+wBAi5uy+Kzw0paqQgIWdt1Fra0+ZRZSC2d3MKo\nukuMFiz1ufzAcTwwa7lSVdn1rAy3o/0uTXZDG1y5cr/y8wIDAQABoyMwITAOBgNV\nHQ8BAf8EBAMCAgQwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEA\nTOUsUpuQB/v/yWREzdhThiUAT0KJL5MUgDALz3NqS6WfEWtHbCwYDgt5aFjb2BsE\nH7n9ccrbPCGCSFrAWpvOMXzM0l8zXMIKeuU2uX5JabFe3Rg+SDbLltAfWPAZouSh\nfgbvNZQRLFLoOUOXlnTTCSZhokM1W1hvWljjo8FBsssSnbbOzoC/bv86daDfTXaA\nV3AiM+ZkGw6Ef0FStBVNVXGrFf86ESeNexUcbGNYJTPcxkefmssNAHlpZ0WImBAI\nS/Y7+uhfpDtRJT8Y9EBzYCUMVy8kOB5wvJvIIKNElHY360mqcOq+ysukzuSvCiqp\nqTCmTGPhX4vkgUhvzbv99g==\n-----END CERTIFICATE-----"
      token="eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tNTV2bjYiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImZmMzdhMzU4LWE2ZWEtMTFlOC1hNTZkLTQyMDEwYWE4MDAwNCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.oHq_Zkj6kV2YeW8Su_9Uy6AR4Cxt7NdU8T4ACFCXhrqNac2LQfsZ7fPCCRtmVniFa2OiBYAeMwUQMwr5DNgeoOkPEBlMg0tHnRCOxvPwHd2NhF6WfBw7T0TMc6nQH8arsqz9OMPwjfadbas5wjL9jcqkUzxaYpwdaMl2Qwk0yJe7AOGzLFyf3ZAnIIfxdlkUdJQCRyUgNhO5mCIdZMInhUYcQZTPKyvjLGKUtCQ9JRvQAu4r32vYxEGUMQpUkVZliqPFm46oMH1zIhHYo9c6yGpykDLDvMZMz0PUzwp87_7diPTjMUTYerIh5VNxHROhqsqhfuyAWRfTbWtzd5aLwg"
    elif [ "$CI_COMMIT_REF_NAME" == "stag" ]; then
      url="https://35.228.111.249"
      cert="-----BEGIN CERTIFICATE-----\nMIIDCzCCAfOgAwIBAgIQA3cE8zc6r+7wgf8tiC5Y5TANBgkqhkiG9w0BAQsFADAv\nMS0wKwYDVQQDEyQyM2IyN2QyYi0zY2IyLTRlZTItYTZlOC1kZmZmNWEyMjUyYWEw\nHhcNMTgwODIzMDI0MzM1WhcNMjMwODIyMDM0MzM1WjAvMS0wKwYDVQQDEyQyM2Iy\nN2QyYi0zY2IyLTRlZTItYTZlOC1kZmZmNWEyMjUyYWEwggEiMA0GCSqGSIb3DQEB\nAQUAA4IBDwAwggEKAoIBAQCwfvDcY0egUtNQ7c75qebJX1kJJeCWCuzrkJghZPBf\n2yAQKimiGffEFxnArRlMDWf/4n/v4OjhlRf4N0TGRSVeQYsag3rZ6EAxEoloFwa+\n8pjKPykuw9wEUhid48sKUGMqjLFcqlqUeiD2toLg/+2edwQOkLHbQtLuPW2cHG/C\n1tWOs1tBMc6RwKn2fLgsPhLYg/4gK/YLHkD6z4GNlPAsLmxLyYid6p479FVIeZvP\n0gRYUEJbfp0D3YeT4dVokiyqUeF8eaf/2F96l4oGrrCB3q0MnGh98Lixi19zmcj9\nrxTI8IpGIkt0Mi+9sRZIwdJJH5/DF4fd6su8/pVg7jadAgMBAAGjIzAhMA4GA1Ud\nDwEB/wQEAwICBDAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBd\nAVvC31BNUSdorW3o1gT/iRLRXHnPEGDLBWtoxWBSclZcLEkubHk72IjYpdD4rtWf\nsuHr8WJHt0HPt8/bZ/jsiodCSO8rJp5GY1e/DOziMvMwxrfaYCrVPaIxas7UizK4\n++hdzc4efhOUjoL5JMI9/zOZ0IQi2Om+1BhYc9Vz3D1aU2PKWnds3TTftFwZk1zQ\nsg0tt2IX5vA/Ng7HvthEIJFx6xvv55NW/kdCDMdaSulcfRYq8Dq3eHlYZr9i+arJ\ndnkZDHVMU5t2ppD4lNlE5U+KAompKiUkTIjUM3hjb9Hy175jdMB2mpcmfMhumDtZ\nMcJh52/blvHkbPyFTXZp\n-----END CERTIFICATE-----"
      token="eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tdDg5ODQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImY1ZDdjYzQ3LWE2ODYtMTFlOC04YWQyLTQyMDEwYWE2MDAwZiIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.iFPkyF8N4ZciHeMLXMztnSdSt01sDYeDLTWovzltOMiLyha51Yf14hzE7R3TmWtnqnqjK-K3iBtsKiO3fJsl68bi6ZqK0zFeDHzybXZlyf3qt59_jke-VNip3BWh3JSCfmXjDTh6RreEdNTHsqb8EIzaHH5_TY97-0hn9w0pfbDKMq5rDT3ycVT8PNjL_yILryfMX23YK1MitNssF81g1oN59loU5-Izf1gyxRQC-cxTm0_K8-D3NKxjUfUYfmAmvXl0YxjPW2Y3sX8i8Wg0hslNonNwCIHP2WKXNyxlut5UyDLNgRqBExIqG5MiHYpD2LIbvdL88xRXhX3yxeHURw"
    elif [ "$CI_COMMIT_REF_NAME" == "dev" ]; then
      url="https://35.232.123.55"
      cert="-----BEGIN CERTIFICATE-----\nMIIDDDCCAfSgAwIBAgIRANmLepMjnKXS5e+1V4tK9igwDQYJKoZIhvcNAQELBQAw\nLzEtMCsGA1UEAxMkNzIyMzUwOTAtNTllNS00MWJmLWIwNmQtMjQ4YWI3NDllY2M1\nMB4XDTE4MDgyMzE0MzUyMVoXDTIzMDgyMjE1MzUyMVowLzEtMCsGA1UEAxMkNzIy\nMzUwOTAtNTllNS00MWJmLWIwNmQtMjQ4YWI3NDllY2M1MIIBIjANBgkqhkiG9w0B\nAQEFAAOCAQ8AMIIBCgKCAQEAr8nhkd8eNad3DxSy4v6tMFeUzebzjpVis+POoGmW\nSKuE7KqduXFdH7uQr4yrcujs6eSALJc8XFp1h+fWz9+ZUYKyQfYKa7Qd2EADFEQ+\nLImyqmUP8xZ3MevTpdLqreMwagxYUQ8KPj7ffY1j6ECvi6TJx2AcIbDcZBAGZVEh\nnJCnL2bd2r62vD8vo29vbj4MObyI1+oxfjb1o4qgJJSKNbtdzXE4WH3T0kxcTk+B\nG1byyXKxa1NyX7V7dTtGKR8FfCDCc4btx2s7Sg8NCS48usLSpg+jGccbGiLPCRxw\nH5yKU8YKn9911Haac9SfgwJHVC1nodONytkXU0zwHWqqdwIDAQABoyMwITAOBgNV\nHQ8BAf8EBAMCAgQwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEA\neZWgzaKmYMQ6lkEEttZcQRs9+RFsNMkqvSnNwiktrNk1lxY5LRGOwJkGL+asiar7\nOXOmbO1kWeLVPyT4WAyNvsgoV1x6YI2ktdsZBPiaKcrIXPMqq9p6hZZq9kg5sZA2\nAla4yMe7O4jmwVNfuqZm+I1gyXc9uQ0NeB4dXN1TLq5zMzp9zAYLzUrX/OVaGyL5\nkLjXvt8DPNUylDsI0vbekNvys3nCp7p1Mfyz7a5De2Ze4RdzcVInp2LPhCFcvQp1\nE/h/g84EQwvcJGxEnuwU2NpLrlD4MZ54B8/OuwH8gvWyJ5PYMUlwn+pMug5VPEUg\nmKF8AhdB7id15C8lN/iNxA==\n-----END CERTIFICATE-----"
      token="eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tbWp4amYiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjVjYzQ5NzZkLWE2ZWEtMTFlOC04NDAyLTQyMDEwYTgwMDE3OCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.ULwoVd6LPGlTJJ0qZGVG6SUrJ2I0Yk4K3YlOQTO4dgmxun8FShfp4cICt4HinhqO8epsPkWpTkoHgxSmD_dSuw6g84re9BGEAWr20pbtCLqEKaEllwa49dEopP6fBEeHtAfVAUzNWIOOlkhNCAPYvCCPOuvEFfkADwcaJvE-3ekGysjNRD6Rw7WqgcVLj3KkoK4myKh03rUYr4KO2EwSCTNwubwfPvoSdhw_hzfuLmEnW0q84fEhJzdkQhRSmW1RX9k7HVyTcNY175Qx3OQ3N1A8ZKCY_dIck_G4Ri1NgIyTBy3IL-nIsa5vO06QCD0ppLt7WA1eoToaszSX9im_qQ"
    else
      exit 1
    fi
    export KUBE_URL="$url"
    echo -e "$cert" > /builds/ericssonedge/$CI_PROJECT_NAME.tmp/KUBE_CA_PEM_FILE
    export KUBE_CA_PEM=`cat /builds/ericssonedge/$CI_PROJECT_NAME.tmp/KUBE_CA_PEM_FILE`
    export KUBE_TOKEN="$token"
    cp kube_config.$CI_COMMIT_REF_NAME /builds/ericssonedge/$CI_PROJECT_NAME.tmp/KUBECONFIG
  }


  function build_chart() {
    helm init --client-only
    helm dependency update unsee/
    helm dependency build unsee/
  }

  function set_deploy_variables() {
    export KUBE_NAMESPACE=$KUBE_NAMESPACE-$CI_COMMIT_REF_SLUG
    export TILLER_NAMESPACE=$KUBE_NAMESPACE
  }

  function ensure_namespace() {
    kubectl get namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  function check_kube_domain() {
    if [ -z ${AUTO_DEVOPS_DOMAIN+x} ]; then
      echo "In order to deploy, AUTO_DEVOPS_DOMAIN must be set as a variable at the group or project level, or manually added in .gitlab-cy.yml"
      false
    else
      true
    fi
  }

  function install_tiller() {
    echo "Checking Tiller..."
    helm init --upgrade
    kubectl rollout status -n "$TILLER_NAMESPACE" -w "deployment/tiller-deploy"
    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }

before_script:
  - *auto_devops
